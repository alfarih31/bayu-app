import React from "react";
import {Switch, Route, Router} from 'react-router';
import history from "./history";

import HomePage from "./views/HomePage";
import LoginPage from "./views/LoginPage";

export default function AppRouter () {
  return(
    <>
      <Router history={history}>
        <Switch>
          <Route exact path="/" component={HomePage}/>
          <Route exact path="/login" component={LoginPage}/>
        </Switch>
      </Router>
    </>
  )
}
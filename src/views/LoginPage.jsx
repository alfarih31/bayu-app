import React, {Component} from 'react';
import './LoginPageStyle.css';

export default class LoginPage extends Component {
  constructor() {
    super();

    this.state = {
      userName: undefined,
      password: undefined,
      submitted: false
    }

    this.handleInputUsername = this.handleInputUsername.bind(this);
    this.handleInputPassword = this.handleInputPassword.bind(this);
    this.handleSubmitButton = this.handleSubmitButton.bind(this);
  }

  handleInputUsername(e) {
    this.setState({userName: e.target.value})
  }

  handleInputPassword(e) {
    this.setState({password: e.target.value})
  }

  handleSubmitButton() {
    this.setState({submitted: true});
  }

  render() {
    return (
      <div>
        <div className="Container-Card Shadow-Red">
          <h1 className="Title">
            Login
          </h1>

          <div className="FormGroup">
            <div className="Form-Username">
              <div className="Label">
                Username
              </div>
              <div className="Input-Username">
                <input type="text" placeholder="Please input your username" onChange={this.handleInputUsername}/>
              </div>
            </div>

            <div className="Form-Password">
              <div className="Label">
                Password
              </div>
              <div className="Input-Password">
                <input type="password" placeholder="Please input your password" onChange={this.handleInputPassword}/>
              </div>
            </div>

            <div className="Form-Submit">
              <button className="Button-Submit" onClick={this.handleSubmitButton}>
                <div className="Text-Login">
                  Login
                </div>
              </button>
            </div>
          </div>
        </div>

        <div className="Container-Card Shadow-Blue">
          <p>
            <span style={{fontWeight: 'bold'}}>
              You input username:
            </span>
            {this.state.userName}
          </p>
          <p>
            <span style={{fontWeight: 'bold'}}>
              You input password:
            </span>
            {this.state.password}
          </p>

          <p>
            <span style={{fontWeight: 'bold'}}>
              Is submmitted:
            </span>
            {this.state.submitted ? 'Submitted' : 'Not-Submitted'}
          </p>
        </div>
      </div>
    )
  }
}
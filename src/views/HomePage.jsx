import React from 'react';
import {Container, Grid, Paper} from "@material-ui/core";

export default class HomePage extends React.Component {
  render() {
    return(
      <>
        <Container>
          <Grid container spacing={1}>
            <Grid item xs={3}>
              <Paper>
                XS = 3
              </Paper>
            </Grid>
            <Grid item xs={9}>
              <Paper>
                XS = 9
              </Paper>
            </Grid>
          </Grid>
        </Container>
      </>
    )
  }
}